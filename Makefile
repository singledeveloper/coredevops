include .env
export $(shell sed 's/=.*//' .env)

up:
	./dc.sh up -d
	@make refresh-scheduler

stop:
	@echo "stopping";
	./dc.sh stop nginx php-fpm
	@make stop-scheduler
	@make stop-worker container=worker
	./dc.sh stop
	@echo "done"

stop-worker:
	./dc.sh kill -s SIGUSR2 $(container)
	./dc.sh kill -s SIGTERM $(container)
	while make dcexec container=$(container) arg='ps aux' | grep "[q]ueue:work"; do \
	sleep 1; \
	done;
	./dc.sh stop $(container)

stop-scheduler:
	@make remove-scheduler
	while make dcexec container=scheduler arg='ps aux' | grep "[s]chedule:run"; do \
	sleep 1; \
	done;
	./dc.sh stop scheduler

refresh-scheduler:
	@make remove-scheduler
	@make dcexec container=scheduler arg='/usr/bin/crontab /etc/crontab'

remove-scheduler:
	@make dcexec container=scheduler arg="crontab -d"

dcexec:
	./dc.sh exec $(container) sh -c "$(arg)"

ps:
	./dc.sh ps

exec-nginx:
	./dc.sh exec nginx bash

reload-nginx:
	@make dcexec container=nginx arg='nginx -s reload'

exec-workspace:
	./dc.sh exec workspace bash -c "cd odeo-core; exec bash"

migrate-workspace:
	@make dcexec container=workspace arg='php /var/www/odeo-core/artisan migrate'

reload-opcache:
	@make dcexec container=php-fpm arg='kill -USR2 1'

restart-queue:
	@make dcexec container=workspace arg='php /var/www/odeo-core/artisan queue:restart'

up-jabber: export APP_ENV=JABBER
up-jabber:
	./dc.sh up -d jabber-listener jabbr-listener worker-jabber

stop-jabber: export APP_ENV=JABBER
stop-jabber:
	./dc.sh stop jabber-listener
	@make stop-jabber-worker container=jabber-worker

ps-jabber: export APP_ENV=JABBER
ps-jabber:
	./dc.sh ps

stop-jabber-worker: export APP_ENV=JABBER
stop-jabber-worker: stop-worker
