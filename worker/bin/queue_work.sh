#!/bin/sh

set -e

WORKER_ENV=${WORKER_ENV:-"PRODUCTION"}

if [ "$WORKER_ENV" = "LOCAL" ]; then
  php /var/www/odeo-core/artisan queue:listen redis --queue=global --tries=1
else
  php /var/www/odeo-core/artisan queue:work redis --queue=global --tries=1 --daemon
fi